package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class SkeletonFactory<T> implements Skeleton {
    private final MessageManager messageManager;
    private final NetworkAddress networkAddress;
    private final T object;
    private final Runnable runnable;


    public SkeletonFactory(T object) {
        this.object = object;
        messageManager = new MessageManager();
        networkAddress = messageManager.getMyAddress();
        runnable = () -> {
            while (true) {
                MethodCallMessage request = messageManager.wReceive();
                handleRequest(request);
            }
        };
    }

    public static Object createSkeleton(Object testImplementation) {
        try {
            return SkeletonFactory.class.getDeclaredConstructor(Object.class).newInstance(testImplementation);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void run() {
        Thread thread = new Thread(runnable);
        thread.start();
    }

    @Override
    public NetworkAddress getAddress() {
        return networkAddress;
    }

    @Override
    public void handleRequest(MethodCallMessage message) {
        String methodName = message.getMethodName();
        boolean methodFound = false;
        for (int i = 0; i < object.getClass().getMethods().length; i++) {
            if (object.getClass().getMethods()[i].getName().equals(methodName)) {
                methodFound = true;
                processMessage(i,message);
            }
        }
        if (!methodFound) {
            throw new RuntimeException();
        }
    }

    public void processMessage(int i, MethodCallMessage message){
        try {
            Method method = object.getClass().getMethods()[i];
            method.setAccessible(true);
            Object[] args = new Object[method.getParameterCount()];
            int teller = 0;
            for (int j = 0; j + teller < message.getParameters().size(); j++) {
                T parameter = (T) message.getParameter("arg" + j);
                if (parameter != null) {
                    args[j] = (T) castPrimitive(method.getParameters()[j], message.getParameters().get(method.getParameters()[j].getName()));
                } else {
                    T parameterInstance = (T) method.getParameters()[j].getType().getDeclaredConstructor().newInstance();
                    for (int k = 0; k < parameterInstance.getClass().getDeclaredFields().length; k++) {
                        Field field = parameterInstance.getClass().getDeclaredFields()[k];
                        field.setAccessible(true);
                        String value = message.getParameter("arg" + j + "." + field.getName());
                        if (field.getType().equals(Integer.TYPE)) {
                            field.setInt(parameterInstance, Integer.parseInt(value));
                        } else if (field.getType().equals(Character.TYPE)) {
                            field.set(parameterInstance, value.charAt(0));
                        } else if (field.getType().equals(Boolean.TYPE)) {
                            field.set(parameterInstance, Boolean.parseBoolean(value));
                        } else {
                            field.set(parameterInstance, value);
                        }
                        teller++;
                    }
                    teller--;
                    args[j] = parameterInstance;
                }
            }
            invokeAndSend(method, args, message);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }


    public void invokeAndSend(Method method, Object[] args, MethodCallMessage message) {
        try {
            T result = (T) method.invoke(object, args);
            MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "reply");
            if (result == null) {
                reply.setParameter("result", "Ok");
            } else if (result instanceof String || result instanceof Integer || result instanceof Double || result instanceof Boolean || result instanceof Character) {
                reply.setParameter("result", result.toString());
            } else {
                for (Field field : result.getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    reply.setParameter("result." + field.getName(), field.get(result).toString());
                }
            }
            messageManager.send(reply, message.getOriginator());
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    public Object castPrimitive(Parameter parameter, String reply) {
        try {
            if (parameter.getType().equals(Integer.TYPE)) {
               return Integer.parseInt(reply);
            } else if (parameter.getType().equals(Double.TYPE)) {
                return Double.parseDouble(reply);
            } else if (parameter.getType().equals(String.class)) {
                return reply;
            } else if (parameter.getType().equals(Character.TYPE)) {
                return reply.charAt(0);
            } else if (parameter.getType().equals(Boolean.TYPE)) {
                return Boolean.parseBoolean(reply);
            }
            return null;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
