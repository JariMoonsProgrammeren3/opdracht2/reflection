package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MyInvocationHandler<T> implements InvocationHandler {
    private NetworkAddress networkAddress;
    private MessageManager messageManager;

    public MyInvocationHandler(NetworkAddress networkAddress) {
        this.networkAddress = networkAddress;
        messageManager = new MessageManager();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return send(method, (T[]) args);
    }

    public T send(Method method, T[] args) throws Exception {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(),method.getName());
        for (int i = 0; i < method.getParameterCount(); i++){
            if (args[i] instanceof String || args[i] instanceof Integer || args[i] instanceof Boolean || args[i] instanceof Character || args[i] instanceof Double){
                message.setParameter("arg" + i,args[i].toString());
            } else {
                for (int j = 0; j < args[i].getClass().getDeclaredFields().length; j++){
                    Field field = args[i].getClass().getDeclaredFields()[j];
                    field.setAccessible(true);
                    message.setParameter("arg" + i + "." + field.getName(),field.get(args[i]).toString());
                }
            }
        }
        messageManager.send(message, networkAddress);
        MethodCallMessage reply = messageManager.wReceive();
        return (T) cast(method, reply);
    }

    public Object cast(Method  method, MethodCallMessage reply) throws Exception {
       if (method.getReturnType().equals(Integer.TYPE)){
            return Integer.parseInt(reply.getParameter("result"));
       }else if(method.getReturnType().equals(String.class)){
           return reply.getParameter("result");
       }else if (method.getReturnType().equals(Double.TYPE)){
           return Double.parseDouble(reply.getParameter("result"));
       } else if (method.getReturnType().equals(Character.TYPE)){
           return reply.getParameter("result").charAt(0);
       }else if(method.getReturnType().equals(Boolean.TYPE)) {
           return Boolean.parseBoolean(reply.getParameter("result"));
        }else if (reply.getParameters().size() > 1){
           T object = (T) method.getReturnType().getDeclaredConstructor().newInstance();
           for (int i = 0; i < object.getClass().getDeclaredFields().length; i++){
              Field field = object.getClass().getDeclaredFields()[i];
              field.setAccessible(true);
              if (field.getType().equals(Integer.TYPE)){
                  field.set(object,Integer.parseInt(reply.getParameters().get("result." + field.getName())));
              }else if (field.getType().equals(Character.TYPE)){
                  field.set(object,reply.getParameters().get("result." + field.getName()).charAt(0));
              }else if (field.getType().equals(Boolean.TYPE)){
                  field.set(object,Boolean.parseBoolean(reply.getParameters().get("result." + field.getName())));
              } else {
                  field.set(object,reply.getParameters().get("result." + field.getName()));
              }
           }
           return object;
       }
        return null;
    }
}
