package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Proxy;

public class StubFactory {
    public static Object createStub(Class testInterfaceClass, String s, int port) {
        NetworkAddress networkAddress = new NetworkAddress(s,port);
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),new Class[]{testInterfaceClass},new MyInvocationHandler(networkAddress));
    }
}
